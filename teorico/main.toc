\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Amplificadores Realimentados}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Tipos de amplificadores}{3}{section.1.1}
\contentsline {subsection}{Amplificador de Tension no realimentado}{3}{section*.9}
\contentsline {subsection}{Amplificador de Corriente no realimentado}{3}{section*.10}
\contentsline {subsection}{Amplificador de Transconductancia no realimentado}{4}{section*.11}
\contentsline {subsection}{Amplificador de Transresistencia no realimentado}{4}{section*.12}
\contentsline {subsection}{Comparaci\IeC {\'o}n de amplificadores}{4}{section*.13}
\contentsline {section}{\numberline {1.2}El amplificador realimentado}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Ganancia y De-sensibilidad}{5}{section.1.3}
\contentsline {subsection}{Desarrollo por relaci\IeC {\'o}n entre salida y entrada}{5}{section*.14}
\contentsline {subsection}{Desarrollo por relaci\IeC {\'o}n de derivadas}{5}{section*.15}
\contentsline {section}{\numberline {1.4}Tipos de Realimentaci\IeC {\'o}n}{6}{section.1.4}
\contentsline {section}{\numberline {1.5}An\IeC {\'a}lisis de amplificadores realimentados}{6}{section.1.5}
\contentsline {subsection}{Amplificador Tension realimentado en serie}{6}{section*.16}
\contentsline {subsection}{Amplificador de Corriente realimentado en serie}{7}{section*.17}
\contentsline {subsection}{Amplificador de Corriente realimentado en paralelo}{8}{section*.18}
