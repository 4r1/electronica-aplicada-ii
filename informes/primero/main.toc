\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Materiales e instrumental necesarios}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Introducci\IeC {\'o}n}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Consignas}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Resoluci\IeC {\'o}n}{3}{section.2.2}
\contentsline {subsection}{Determinaci\IeC {\'o}n de la impedancia de salida del amplificador en lazo abierto y cerrado ($Z_o$ y $Z_{of}$ respectivamente)}{3}{section*.3}
\contentsline {subsubsection}{Resultados}{3}{section*.5}
\contentsline {subsection}{Determinaci\IeC {\'o}n de la impedancia de entrada del amplificador en lazo abierto y cerrado ($Z_i$ y $Z_{if}$ respectivamente)}{4}{section*.6}
\contentsline {subsubsection}{Resultados}{4}{section*.8}
\contentsline {subsection}{Determinaci\IeC {\'o}n de las ganancias de tensi\IeC {\'o}n en lazo abierto y cerrado}{5}{section*.9}
\contentsline {subsection}{Determinaci\IeC {\'o}n de la desensibilidad}{6}{section*.10}
\contentsline {subsection}{Respuesta en frecuencia}{7}{section*.11}
\contentsline {subsection}{Simulaciones}{8}{section*.14}
\contentsline {chapter}{\numberline {3}Conclusi\IeC {\'o}n}{10}{chapter.3}
