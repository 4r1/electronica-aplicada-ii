\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Pautas}{2}{section.1.2}
\contentsline {chapter}{\numberline {2}Desarrollo}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Diagrama en bloques}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Referencia de Tension}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Primera Etapa (Desplazador)}{7}{section.2.3}
\contentsline {subsection}{Circuito}{7}{section*.2}
\contentsline {subsection}{C\IeC {\'a}lculos}{7}{section*.3}
\contentsline {subsection}{Impedancia de Entrada}{8}{section*.4}
\contentsline {subsection}{Simulaci\IeC {\'o}n}{8}{section*.5}
\contentsline {section}{\numberline {2.4}Segunda Etapa (Integrador)}{9}{section.2.4}
\contentsline {subsection}{Circuito}{9}{section*.6}
\contentsline {subsection}{C\IeC {\'a}lculos}{9}{section*.7}
\contentsline {subsection}{Simulaci\IeC {\'o}n}{12}{section*.8}
\contentsline {section}{\numberline {2.5}Tercera Etapa (Derivador)}{12}{section.2.5}
\contentsline {subsection}{Circuito}{12}{section*.9}
\contentsline {subsection}{C\IeC {\'a}lculos}{13}{section*.10}
\contentsline {subsection}{Simulaci\IeC {\'o}n}{15}{section*.11}
\contentsline {section}{\numberline {2.6}Cuarta Etapa (Derivador y Rectificador)}{15}{section.2.6}
\contentsline {subsection}{Circuito}{15}{section*.12}
\contentsline {subsection}{C\IeC {\'a}lculos}{15}{section*.13}
\contentsline {subsection}{Impedancia de Salida}{16}{section*.14}
\contentsline {subsection}{Simulaci\IeC {\'o}n}{17}{section*.15}
\contentsline {chapter}{\numberline {3}Conclusiones}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}TL07(2/4)}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}TL431}{19}{section.3.2}
\contentsline {section}{\numberline {3.3}Observaciones y Conclusion}{21}{section.3.3}
