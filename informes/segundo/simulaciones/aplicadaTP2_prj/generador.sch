<Qucs Schematic 0.0.19>
<Properties>
  <View=0,0,908,874,1,0,0>
  <Grid=10,10,1>
  <DataSet=generador.dat>
  <DataDisplay=generador.dpl>
  <OpenDisplay=1>
  <Script=generador.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
  <Line -10 -50 20 0 #000080 2 1>
  <Line 0 -50 0 -10 #000080 2 1>
  <Line -10 50 20 0 #000080 2 1>
  <Line 0 60 0 -10 #000080 2 1>
  <.PortSym 0 60 1 180>
  <Line 40 20 0 -40 #000080 2 1>
  <.PortSym 0 -60 2 0>
  <.ID 50 -60 GENERADOR>
  <Line -40 20 0 -40 #000080 2 1>
  <Rectangle -40 -50 80 100 #000000 0 1 #c0c0c0 1 0>
  <Text -10 30 12 #000000 0 "GND">
  <Text -20 -50 12 #000000 0 "SIGN">
</Symbol>
<Components>
  <GND * 1 180 760 0 0 0 0>
  <Vdc V1 1 240 740 -26 -56 0 2 "1.25 V" 1>
  <Port MASA 1 180 740 -23 -50 1 0 "1" 1 "analog" 0>
  <Port SEÑAL 1 370 740 4 -50 0 2 "2" 1 "analog" 0>
  <Vrect e1 1 340 740 -26 18 0 0 "2.5 V" 1 "0.116 ms" 1 "0.116 ms" 1 "1 ns" 1 "1 ns" 1 "0 ns" 0>
</Components>
<Wires>
  <180 740 180 760 "" 0 0 0 "">
  <180 740 210 740 "" 0 0 0 "">
  <270 740 310 740 "" 0 0 0 "">
  <370 740 370 740 "salida" 400 710 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
