<Qucs Schematic 0.0.19>
<Properties>
  <View=66,120,1803,745,0.911917,0,0>
  <Grid=10,10,1>
  <DataSet=proyecto2Final.dat>
  <DataDisplay=proyecto2Final.dpl>
  <OpenDisplay=1>
  <Script=proyecto2Final.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.TR TR1 1 290 530 0 69 0 0 "lin" 1 "10 ms" 1 "11 ms" 1 "5432" 1 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <GND * 1 150 360 0 0 0 0>
  <GND * 1 380 390 0 0 0 0>
  <GND * 1 290 410 0 0 0 0>
  <Vdc V1 1 290 370 18 -26 0 1 "5 V" 1>
  <Sub SUB2 1 280 300 -26 88 0 0 "sumador_inversor_invertido.sch" 1>
  <GND * 1 640 490 0 0 0 0>
  <Vdc V2 1 640 450 18 -26 0 1 "5 V" 1>
  <GND * 1 720 460 0 0 0 0>
  <GND * 1 900 570 0 0 0 0>
  <Vdc V3 1 900 530 18 -26 0 1 "5 V" 1>
  <GND * 1 990 570 0 0 0 0>
  <Sub SUB3 1 750 370 -146 88 0 0 "integrador_invesor_invertido.sch" 1>
  <GND * 1 1410 680 0 0 0 0>
  <Sub SUB4 1 990 450 -26 88 0 0 "primer_derivador_inversor_con-inversor.sch" 1>
  <Sub SUB1 1 150 260 -26 68 0 0 "generador.sch" 1>
  <R R1 1 1610 620 15 -26 0 1 "10k Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <GND * 1 1610 670 0 0 0 0>
  <Sub SUB5 1 1410 580 -26 68 0 0 "segundo_derivador_inversor_con-inversor.sch" 1>
</Components>
<Wires>
  <150 320 150 360 "" 0 0 0 "">
  <150 190 150 200 "" 0 0 0 "">
  <150 190 390 190 "generador" 270 160 92 "">
  <390 190 390 210 "" 0 0 0 "">
  <380 380 380 390 "" 0 0 0 "">
  <290 400 290 410 "" 0 0 0 "">
  <290 320 290 340 "" 0 0 0 "">
  <290 320 320 320 "" 0 0 0 "">
  <640 480 640 490 "" 0 0 0 "">
  <640 400 640 420 "" 0 0 0 "">
  <640 400 670 400 "" 0 0 0 "">
  <720 450 720 460 "" 0 0 0 "">
  <460 290 720 290 "signal1" 540 250 42 "">
  <720 290 720 330 "" 0 0 0 "">
  <770 390 990 390 "signal2" 840 360 38 "">
  <900 560 900 570 "" 0 0 0 "">
  <900 480 900 500 "" 0 0 0 "">
  <900 480 930 480 "" 0 0 0 "">
  <990 530 990 570 "" 0 0 0 "">
  <1410 460 1410 500 "" 0 0 0 "">
  <1410 640 1410 680 "" 0 0 0 "">
  <1050 460 1410 460 "signal3" 1120 430 42 "">
  <1610 650 1610 670 "" 0 0 0 "">
  <1610 570 1610 590 "" 0 0 0 "">
  <1470 570 1610 570 "signal4" 1540 540 36 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
