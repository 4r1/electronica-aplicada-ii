\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Condiciones de dise\IeC {\~n}o}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Mediciones realizadas}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Instrumental utilizado}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Circuito amplificador de instrumentaci\IeC {\'o}n}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Mediciones}{3}{section.2.2}
\contentsline {subsection}{Relaci\IeC {\'o}n de rechazo al modo com\IeC {\'u}n y Ganancias}{3}{section*.3}
\contentsline {subsubsection}{Resultados}{4}{section*.6}
\contentsline {subsection}{Ancho de Banda}{4}{section*.7}
\contentsline {chapter}{\numberline {3}Conclusion}{5}{chapter.3}
